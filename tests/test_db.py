# LBOT is a IRC bot that can serve RSS feed in your channel. no copyright. no LICENSE.
#
# database tests.

import lo
import unittest

class Test_Store(unittest.TestCase):

    def test_emptyargs(self):
        db = lo.dbs.Db()
        res = list(db.find("", {}))
        self.assertEqual(res, [])
