# LBOT is a IRC bot that can serve RSS feed in your channel. no copyright. no LICENSE.
#
# tests attributes on Objects

import lo
import time
import unittest

class Test_Attribute(unittest.TestCase):

    def timed(self):
        with self.assertRaises((AttributeError, )):
            o = lo.Object()
            o.timed2

    def timed2(self):
        o = lo.Object()
        o.date = time.ctime(time.time())
        self.assert_(o.timed())
