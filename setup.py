# LBOT is a IRC bot that can serve RSS feed in your channel. no copyright. no LICENSE.
#
# setup.py

from setuptools import setup, find_namespace_packages

setup(
    name='lbot',
    version='5',
    url='https://bitbucket.org/botd/lbot',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description="LBOT is a IRC bot that can serve RSS feed in your channel. no copyright. no LICENSE.",
    long_description="""
R E A D M E
###########


LBOT is a IRC bot that can serve RSS feeds to your channel. no copyright. no LICENSE.


I N S T A L L


download the tarball from pypi, https://pypi.org/project/lbot/#files


you can also download with pip3 and install globally.

::

 > sudo pip3 install lbot --upgrade

if you want to develop on the library clone the source at bitbucket.org:

::

 > git clone https://bitbucket.org/botd/lbot
 > cd lbot
 > sudo python3 setup.py install

if you want to have the daemon started at boot, run:

::

 > sudo bin/install

this will install an lbot service in /etc/systemd/system


U S A G E


::

 > lbot <cmd>
 > lbot -m irc,rss localhost \#dunkbots lbot
 > lbot -s 
 > lbot -d 

logfiles can be found in ~/.lbot/logs. you can use the --owner option to set the owner of the bot to your own userhost.


C O N F I G U R A T I O N


you can use the lbot program option to configure LBOT:

::

 > lbot cfg krn modules rss,udp,irc
 > lbot cfg irc server localhost
 > lbot cfg irc channel #dunkbots
 > lbot cfg irc nick lbot
 > lbot meet ~bart@127.0.0.1
 > lbot rss rss https://news.ycombinator.com/rss

use the -w option if you want to use a different work directory then ~/.lbot, for example:

::

 > sudo lbot -w /var/lib/lbot -a /var/log/lbot cfg irc server irc.freenode.net


R S S


start the rss module by adding rss to the modules option at start:

::

 > lbot -m rss,irc 

add an url:

::

 > lbot rss https://news.ycombinator.com/rss
 ok 1

 run the rss commad to see what urls are registered:

 > lbot rss
 0 https://news.ycombinator.com/rss

 the fetch command can be used to poll the added feeds:

 > lbot fetch
 fetched 0


U D P


using udp to relay text into a channel, start the bot with -m udp and use
the ludp program to send text via the bot to the channel on the irc server:

::

 > tail -f ~/.lbot/logs/lbot.log | ./bin/ludp 


M O D U L E S


LBOT contains the following modules:

::

    lo                          - object library.
    lo.clk                      - clock functions.
    lo.dbs                      - database.
    lo.evt                      - basic event.
    lo.gnr                      - generic object functions.
    lo.hdl                      - handler.
    lo.ldr                      - module loader.
    lo.tms                      - time related functions.
    lo.thr                      - threads.
    lo.typ                      - typing.
    lbot.cfg                    - config command.
    lbot.cmd                    - list of commands.
    lbot.irc                    - irc bot.
    lbot.rss                    - feed fetcher.
    lbot.shl                    - shell code.
    lbot.shw                    - show runtime.
    lbot.udp                    - udp to irc relay
    lbot.usr                    - user management.


C O D I N G


if you want to add your own modules to the bot, you can put you .py files in a "mods" directory and use the -m option to point to that directory.

basic code is a function that gets an event as a argument:

 def command(event):
     << your code here >>

to give feedback to the user use the event.reply(txt) method:

 def command(event):
     event.reply("yooo %s" % event.origin)


have fun coding ;]


you can contact me on IRC/freenode/#dunkbots.

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net

    
    """,
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=True,
    packages=["lo", "lbot"],
    scripts=["bin/lbot", 'bin/ludp'],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
