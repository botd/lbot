# LBOT is a IRC bot that can serve RSS feed in your channel. no copyright. no LICENSE.
#
# find command

import lo
import os
import time

def find(event):
    if not event.args:
        wd = os.path.join(lo.workdir, "store", "")
        lo.cdir(wd)
        fns = os.listdir(wd)
        fns = sorted({x.split(".")[-1].lower() for x in fns})
        if fns:
            event.reply("|".join(fns))
        return
    if not len(event.args) > 1:
        event.reply("find <type> <match>")
        return
    match = event.args[0]
    nr = -1
    db = lo.dbs.Db()
    for o in db.find_value(match, event.args[1]):
        nr += 1
        event.display(o, str(nr))
